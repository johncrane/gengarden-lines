module AppTests exposing (suite)

import Expect
import Fuzz exposing (int, intRange)
import Html exposing (Html, div, text)
import Html.Events exposing (..)
import List exposing (length)
import Main
import Test exposing (..)
import Test.Html.Event as E
import Test.Html.Query as Q
import Test.Html.Selector as S
import Time exposing (millisToPosix)
import Tuple exposing (first, second)


{-|

    tileTests : Test
    tileTests =
        describe "Tile tests"
            [ fuzz2
                (intRange -500 500)
                (intRange 1000 2000)
                "different seeds will generate different listOfRandomInts"
              <|
                \seed1 seed2 ->
                    listOfRandomInts 20 seed1
                        |> Expect.notEqual (listOfRandomInts 20 seed2)
            ]

-}
suite : Test
suite =
    describe "Views"
        [ test "no fail" <|
            \() ->
                Expect.equal 0 0
        ]
