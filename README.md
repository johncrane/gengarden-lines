# Gen Garden - Flying Lines
A generative art framework written in Elm 0.19

See the output [here](https://johncrane.gitlab.io/gengarden-lines/).

Portions of this code are taken from [carwow/elm-slider](https://github.com/carwow/elm-slider/tree/7.0.1) package under the BSD 3 Clause license. 
